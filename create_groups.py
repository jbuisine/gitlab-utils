import os
import argparse
import gitlab
import config as gl_conf

from models.group import Group

def extract_group_info(group: Group):
    
    group_main, group_name = os.path.split(group.path)
    
    if len(group_main) > 0:
        return group_main, group_name
    
    return None, group_name

def extract_groups(csv_filename):
    
    groups = []
    
    with open(csv_filename, 'r', encoding='utf-8') as f:
        
        for line in f.readlines():
            group_data = line.replace('\n', '').split(',')
            name = group_data[0]
            path = group_data[1]
            description = group_data[2]
            
            # create group
            user = Group(name, path, description)
            
            groups.append(user)

    return groups

def main():
    
    parser = argparse.ArgumentParser(description="Create groups/subgroups")
    parser.add_argument('--csv', type=str, help="CSV with expected format", required=True)

    args = parser.parse_args()

    csv_file = args.csv
    
    group_data = extract_groups(csv_filename=csv_file)
    
    # private token or personal token authentication (self-hosted GitLab instance)
    gl = gitlab.Gitlab(url=gl_conf.GITLAB_URL, private_token=gl_conf.GITLAB_TOKEN)
    # gl.enable_debug()
    known_groups = gl.groups.list()
    
    print('\nAuthentication is ok!')    
    for group in group_data:
        
        # check if already exists
        if any([ g.full_path == group.path for g in known_groups ]):
            print(f' - Group already exists: {group}')
            continue
        
        print(f' - Create group from: {group}')
        main_group_path, group_path = extract_group_info(group)
        
        if main_group_path is not None:
            
            # need to refresh request
            # TODO: cache can be used
            current_groups = gl.groups.list()
                
            # need to find main group id (from known group)
            parent_group_id = None
            
            for known_group in current_groups:
                
                # use of `full_path` attribute
                if known_group.full_path == main_group_path:
                    parent_group_id = known_group.id
            
            if parent_group_id is not None:            
                gl.groups.create({'name': group.name, 'path': group_path, 'description': group.description, 
                                'parent_id': parent_group_id})
            else:
                print('[Error] parent group not found...')
        else:
            # classical main group
            gl.groups.create({'name': group.name, 'path': group.path, 'description': group.description})
            
    groups = gl.groups.list()

    print(groups)
        
if __name__ == "__main__":
    main()
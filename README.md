# Gitlab groups/users creation

## Description

Enable to create all groups/subgroups and users on Gitlab instance from a single CSV file.
## Installation

### Dependencies:
```
pip install python-gitlab unidecode
```

**or**

```
pip install -r requirements.txt
```

### Configuration:
Use of specific gitlab config:
```
cp config.example.py config.py
```

**Note:** you can create your own access token with "API" checked ([https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))


## Usage

Groups creation:
```
python create_groups.py --csv example_groups.csv
```

Users creation:
```
python create_users.py --csv example_users.csv
```

**Note:** groups can be added later. You only need to rerun the script with new specified groups.
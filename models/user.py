from typing import List

class User():
    """Simple Gitlab user representation
    """
    
    def __init__(self, email: str, username: str, firstname: str, lastname: str, birthdate: str) -> None:
        
        # TODO: manage potential homonyms
        self._email = email
        self._username = username
        self._firstname = firstname 
        self._lastname = lastname
        self._birthdate = birthdate
        self._groups = []

    @property
    def email(self) -> str:
        return self._email
            
    @property
    def username(self) -> str:
        return self._username
    
    @property
    def firstname(self) -> str:
        return self._firstname
    
    @property
    def lastname(self) -> str:
        return self._lastname
    
    @property
    def birthdate(self) -> str:
        return self._birthdate
    
    @property
    def groups(self) -> List[str]:
        return self._groups
    
    def add_group(self, group_path: str):
        """Expected group path associated to the user

        Args:
            group_path (str): group path inside Gitlab
        """
        self._groups.append(group_path)
    
    def __repr__(self):
        return self.__str__()
    
    def __str__(self) -> str:
        return f'{self._username} ({self._firstname} {self._lastname}), '\
            f'email: {self._email}, birthdate: {self._birthdate}, '\
            f'groups: {self._groups}'
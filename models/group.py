class Group():
    """Simple Group representation
    """
    
    def __init__(self, name: str, path: str, description: str) -> None:
        
        # TODO: manage potential homonyms
        self._name = name
        self._path = path 
        self._description = description
        
    @property
    def name(self) -> str:
        return self._name
    
    @property
    def path(self) -> str:
        return self._path
    
    @property
    def description(self) -> str:
        return self._description
        
    def __repr__(self):
        return self.__str__()
    
    def __str__(self) -> str:
        return f'{self._name} (path: {self._path}, description: {self._description})'
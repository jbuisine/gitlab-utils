import argparse
import gitlab
from unidecode import unidecode
import config as gl_conf

from models.user import User

def extract_users(csv_filename):
    
    users = []
    
    with open(csv_filename, 'r', encoding='utf-8') as f:
        
        for line in f.readlines():
            user_data = line.replace('\n', '').split(',')
            email = user_data[0]
            firstname = user_data[1]
            lastname = user_data[2]
            birthdate = user_data[3]
            groups = user_data[4].split(':')
            
            # create username
            username = f'{unidecode(firstname[0].lower())}{unidecode(lastname.strip().replace(" ", "").lower())}'
            user = User(email, username, firstname, lastname, birthdate)
            
            for group in groups:
                user.add_group(group)
            users.append(user)

    return users

def main():
    
    parser = argparse.ArgumentParser(description="Create users accounts with associated groups/subgroups")
    parser.add_argument('--csv', type=str, help="CSV with expected format", required=True)

    args = parser.parse_args()

    csv_file = args.csv
    
    user_data = extract_users(csv_filename=csv_file)
    
    # private token or personal token authentication (self-hosted GitLab instance)
    gl = gitlab.Gitlab(url=gl_conf.GITLAB_URL, private_token=gl_conf.GITLAB_TOKEN)

    # if gl.auth():
    print('Authentication is ok!')    
        
    for user in user_data:
        user_list = gl.users.list(username=user.username)
        print(user_list[0].attributes)
        if len(user_list) > 0:
            print(f' - User already exists: {user}')
            gl_user = user_list[0]
        else:
            print(f' - Create user: {user}')
        
            gl_user = gl.users.create({'email': user.email,
                                'password': user.birthdate,
                                'username': user.username,
                                'name': f'{user.firstname} {user.lastname}',
                                'can_create_group': False,
                                'is_admin': False})
    
        for group_path in user.groups:
            group_list = gl.groups.list()
            
            found_group = None
                
            for group in group_list:
                if group.full_path == group_path:
                    found_group = group
                        
            if found_group is not None:
                
                if any([ m.username == user.username for m in found_group.members.list() ]):
                    print(f'   - {user.username} already added to group: {group_path}')
                else:
                    found_group.members.create({'user_id': gl_user.id, 'access_level': gitlab.const.AccessLevel.DEVELOPER})
                    print(f'   - Add {user.username} to group: {group_path}')
            else:
                print(f'   - Group with path: {group_path} not found...')
            
        
if __name__ == "__main__":
    main()